This user manuals instructions are specifically for windows operation systems within the MSc lab in the University of Leeds School of Computing.

Downloads
=========
The code provided is hosted within a GitLab repository. The URL has been provided within the Deliverables section of this report. The code can be cloned or downloaded and extracted.

Installation
============
* The project solution (.sln) can be opened with visual studio. 
* The windows version needs changing to whichever version is available on that given machine.
  * Right click on the project and press “Properties”
  * Under Configuration Properties  General
    * Change “Windows SDK Version” to SDK available on that machine

Running
=======
Ensure the program is being ran in “Release” mode when running.

Camera Controls
===============

* **WASD** for camera movement
* **Space** to rise the camera position
* **Left shift** to fall the camera position 
* **Mouse** for camera orientation movement
* **Tab** to release the mouse for use of the GUI
* **Q** to unrelease the mouse
* **ESC** to quit the program
