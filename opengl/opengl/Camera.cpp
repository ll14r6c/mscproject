#include "Camera.h"



Camera::Camera()
{
	// Set default vectors
	position = glm::vec3(0.0f, 0.0f, 0.0f);
	front = glm::vec3(0.0f, 0.0f, -1.0f);
	up = glm::vec3(0.0f, 1.0f, 0.0f);
	right = glm::normalize(glm::cross(front, worldUp));
	worldUp = glm::vec3(0.0f, 1.0f, 0.0f);

	frustumDrawData = {
		// Near face
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,

		// Far face
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,

		// Connection between faces
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,

		// Plane normals 
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f,

	};
}

Camera::~Camera()
{
	glDeleteVertexArrays(1, &fVAO);
	glDeleteBuffers(1, &fVBO);
}

// Move camera based on user input
void Camera::ProcessKeyboard(MOVEMENT direction, float deltaTime)
{
	float velocity = speed * deltaTime;
	if (direction == FORWARD) {
		position += front * velocity;
	}
	if (direction == BACKWARD) {
		position -= front * velocity;
	}
	if (direction == LEFT) {
		position -= right * velocity;
	}
	if (direction == RIGHT) {
		position += right * velocity;
	}
	if (direction == UP) {
		position += worldUp * velocity;
	}
	if (direction == DOWN) {
		position += (-worldUp) * velocity;
	}
}

// Move mouse orientation based on user input
void Camera::ProcessMouseMovement(float xoffset, float yoffset)
{
	if (moving) {
		xoffset *= sens;
		yoffset *= sens;

		yaw += xoffset;
		pitch += yoffset;

		// Ensure yaw is between 0-360
		if (yaw < 0.0f) {
			yaw = 360.0f;
		}
		if (yaw > 360.0f) {
			yaw = 0.0f;
		}


		// Deal with gimble lock
		if (pitch > 89.0f) {
			pitch = 89.0f;
		}
		if (pitch < -89.0f) {
			pitch = -89.0f;
		}

		// Update angles
		calcEulerAngles();
	}
}

void Camera::calcEulerAngles()
{
	// Calculate the new Front vector
	glm::vec3 temp;
	temp.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	temp.y = sin(glm::radians(pitch));
	temp.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	front = glm::normalize(temp);

	// Re-calculate the Right and Up vector
	right = glm::normalize(glm::cross(front, worldUp));
	up = glm::normalize(glm::cross(right, front));
}

void Camera::stopMoving()
{
	moving = false;
}

void Camera::startMoving()
{
	moving = true;
}

// Update the query camera variables to match the main camera
void Camera::updateQuery()
{
	// Deal with the two cameras detatching
	if (!detatched) {
		fov_query = fov_cam;
		pos_query = position;
		front_query = front;
		up_query = up;
		right_query = right;
		yaw_query = yaw;
		pitch_query = pitch;
	}
}

// Method to check if an aabb is inside of the viewing frustum
bool Camera::aabbInFrustum(glm::vec3 aabbMin, glm::vec3 aabbMax)
{
	// For each plane
	for (int i = 0; i < 6; i++) {

		// Calculate the positive vertex
		glm::vec3 res = aabbMin;
		if (frustum_planes[i].normal.x > 0) { res.x = aabbMax.x; }
		if (frustum_planes[i].normal.y > 0) { res.y = aabbMax.y; }
		if (frustum_planes[i].normal.z > 0) { res.z = aabbMax.z; }

		// Test if each box is outside the frustum
		if (frustum_planes[i].distance(res) < 0.0f) {
			return false;
		}
	}

	return true;
}

// Setup the VBO and VAO to draw the viewing frustum
void Camera::drawSetup()
{
	glGenVertexArrays(1, &fVAO);
	glGenBuffers(1, &fVBO);

	glBindVertexArray(fVAO);
	glBindBuffer(GL_ARRAY_BUFFER, fVBO);

	glBufferData(GL_ARRAY_BUFFER, frustumDrawData.size() * sizeof(float), frustumDrawData.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	// cleanup
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

// Fill the frustum draw data with the frustum data for the current positions
void Camera::fillFrustumDrawData()
{
	heightNear = 2.0f * tan(fov_query * 0.5f) * near;
	widthNear = heightNear * 1.77f;
	heightFar = 2.0f * tan(fov_query * 0.5f) * far;
	widthFar = heightFar * 1.77f;

	// Near plane vertices
	glm::vec3 nearTopLeft;
	glm::vec3 nearTopRight;
	glm::vec3 nearBotLeft;
	glm::vec3 nearBotRight;

	// Far plane vertices
	glm::vec3 farTopLeft;
	glm::vec3 farTopRight;
	glm::vec3 farBotLeft;
	glm::vec3 farBotRight;

	glm::vec3 farCenter;
	glm::vec3 nearCenter;

	farCenter = pos_query + front_query * far;
	farTopLeft = farCenter + (up_query * (heightFar * 0.5f)) - (right_query * (widthFar * 0.5f));
	farTopRight = farCenter + (up_query * (heightFar * 0.5f)) + (right_query * (widthFar * 0.5f));
	farBotLeft = farCenter - (up_query * (heightFar * 0.5f)) - (right_query * (widthFar * 0.5f));
	farBotRight = farCenter - (up_query * (heightFar * 0.5f)) + (right_query * (widthFar * 0.5f));

	nearCenter = pos_query + (front_query * near);
	nearTopLeft = nearCenter + (up_query * (heightNear * 0.5f)) - (right_query * (widthNear * 0.5f));
	nearTopRight = nearCenter + (up_query * (heightNear * 0.5f)) + (right_query * (widthNear * 0.5f));
	nearBotLeft = nearCenter - (up_query * (heightNear * 0.5f)) - (right_query * (widthNear * 0.5f));
	nearBotRight = nearCenter - (up_query * (heightNear * 0.5f)) + (right_query * (widthNear * 0.5f));

	// Fill frustum planes
	// top plane
	frustum_planes[0].set(nearTopRight, nearTopLeft, farTopLeft);
	// bottom plane
	frustum_planes[1].set(nearBotLeft, nearBotRight, farBotRight);
	// left plane
	frustum_planes[2].set(nearTopLeft, nearBotLeft, farBotLeft);
	// right plane
	frustum_planes[3].set(nearBotRight, nearTopRight, farBotRight);
	// near plane
	frustum_planes[4].set(nearTopLeft, nearTopRight, nearBotRight);
	// far plane
	frustum_planes[5].set(farTopRight, farTopLeft, farBotLeft);

	// Calculate some points for the normals to be drawn from
	glm::vec3 middleTop1 = (nearTopLeft + nearTopRight + farTopLeft + farTopRight) * 0.25f;
	glm::vec3 middleBot1 = (nearBotLeft + nearBotRight + farBotLeft + farBotRight) * 0.25f;
	glm::vec3 middleLeft1 = (nearTopLeft + farTopLeft + farBotLeft + nearBotLeft) * 0.25f;
	glm::vec3 middleRight1 = (farTopRight + farBotRight + nearTopRight + nearBotRight) * 0.25f;

	// Add the normal for the top, bot, right and left normals for display
	glm::vec3 middleTop2   = middleTop1   + (5.0f * frustum_planes[0].normal);
	glm::vec3 middleBot2   = middleBot1   + (5.0f * frustum_planes[1].normal);
	glm::vec3 middleLeft2  = middleLeft1  + (5.0f * frustum_planes[2].normal);
	glm::vec3 middleRight2 = middleRight1 + (5.0f * frustum_planes[3].normal);

	frustumDrawData = {
		// Near face
		nearBotLeft.x, nearBotLeft.y, nearBotLeft.z,
		nearBotRight.x, nearBotRight.y, nearBotRight.z,
		nearBotRight.x, nearBotRight.y, nearBotRight.z,
		nearTopRight.x, nearTopRight.y, nearTopRight.z,
		nearTopRight.x, nearTopRight.y, nearTopRight.z,
		nearTopLeft.x, nearTopLeft.y, nearTopLeft.z,
		nearTopLeft.x, nearTopLeft.y, nearTopLeft.z,
		nearBotLeft.x, nearBotLeft.y, nearBotLeft.z,

		// Far face
		farBotLeft.x, farBotLeft.y, farBotLeft.z,
		farBotRight.x, farBotRight.y, farBotRight.z,
		farBotRight.x, farBotRight.y, farBotRight.z,
		farTopRight.x, farTopRight.y, farTopRight.z,
		farTopRight.x, farTopRight.y, farTopRight.z,
		farTopLeft.x, farTopLeft.y, farTopLeft.z,
		farTopLeft.x, farTopLeft.y, farTopLeft.z,
		farBotLeft.x, farBotLeft.y, farBotLeft.z,

		// Connection between faces
		nearBotLeft.x, nearBotLeft.y, nearBotLeft.z,
		farBotLeft.x, farBotLeft.y, farBotLeft.z,
		nearBotRight.x, nearBotRight.y, nearBotRight.z,
		farBotRight.x, farBotRight.y, farBotRight.z,
		nearTopRight.x, nearTopRight.y, nearTopRight.z,
		farTopRight.x, farTopRight.y, farTopRight.z,
		nearTopLeft.x, nearTopLeft.y, nearTopLeft.z,
		farTopLeft.x, farTopLeft.y, farTopLeft.z,

		// Draw normals 
		middleTop1.x, middleTop1.y, middleTop1.z,
		middleTop2.x, middleTop2.y, middleTop2.z,
		middleBot1.x, middleBot1.y, middleBot1.z,
		middleBot2.x, middleBot2.y, middleBot2.z,
		middleLeft1.x, middleLeft1.y, middleLeft1.z,
		middleLeft2.x, middleLeft2.y, middleLeft2.z,
		middleRight1.x, middleRight1.y, middleRight1.z,
		middleRight2.x, middleRight2.y, middleRight2.z,

	};
}

// Method to draw the viewing frustum
void Camera::draw(Shader shader)
{
	shader.use();

	// Bind VBO and fill with new data
	glBindBuffer(GL_ARRAY_BUFFER, fVBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, frustumDrawData.size() * sizeof(float), frustumDrawData.data());

	// draw mesh
	glBindVertexArray(fVAO);
	glDrawArrays(GL_LINES, 0, (GLsizei)frustumDrawData.size());
}

// Create a bezier curve benchmark path for the camera to move along
void Camera::createSplineMovement(std::string name, float cp1[3], float cp2[3], float cp3[3], float cp4[3], float spline_time)
{
	spline_move = true;
	time_taken = 0.0f;
	total_time = spline_time;
	percentage = 0.0f;
	lowest_fps = 9999.0f;
	highest_fps = -9999.0f;

	pos1 = glm::vec3(cp1[0], cp1[1], cp1[2]);
	pos2 = glm::vec3(cp2[0], cp2[1], cp2[2]);
	pos3 = glm::vec3(cp3[0], cp3[1], cp3[2]);
	pos4 = glm::vec3(cp4[0], cp4[1], cp4[2]);

	current_benchmark.name = name;
	averages.clear();
}

// Update the cameras position based on a bezier curve path
void Camera::updateSplineMovement(float deltaTime, float frameTime)
{
	// Check for lowest and highest frame times
	if (frameTime > highest_fps) { highest_fps = frameTime; }
	if (frameTime < lowest_fps ) { lowest_fps = frameTime;  }

	// Add time taken to averages array
	averages.push_back(frameTime);

	if (spline_move == true) {
		time_taken += deltaTime;
		percentage = time_taken / total_time;

		// If the recording has ended
		if (time_taken >= total_time) {
			spline_move = false;
			
			// Save recording to arrays
			current_benchmark.min = lowest_fps;
			current_benchmark.max = highest_fps;
			current_benchmark.average = std::accumulate(averages.begin(), averages.end(), 0.0) / averages.size();
			benchmarks.push_back(current_benchmark);
		}
		else {
			// update cam position with bezier curve
			position = getBezierPointAt(percentage);

			// Ensure camera always looks at the origin
			front = glm::vec3(0.0f) - position;

			// Update the new camera vectors
			front = glm::normalize(front);
			right = glm::normalize(glm::cross(front, worldUp));
			up = glm::normalize(glm::cross(right, front));
			updateQuery();
		}
	}
}

// Calculate a point for a bezier path along a certain amount, t
glm::vec3 Camera::getBezierPointAt(float t)
{
	float x = pow(1 - t, 3) * pos1.x + 3 * t * pow(1 - t, 2) * pos2.x + 3 * pow(t, 2) * (1 - t) * pos3.x + pow(t, 3) * pos4.x;
	float y = pow(1 - t, 3) * pos1.y + 3 * t * pow(1 - t, 2) * pos2.y + 3 * pow(t, 2) * (1 - t) * pos3.y + pow(t, 3) * pos4.y;
	float z = pow(1 - t, 3) * pos1.z + 3 * t * pow(1 - t, 2) * pos2.z + 3 * pow(t, 2) * (1 - t) * pos3.z + pow(t, 3) * pos4.z;
	return glm::vec3(x, y, z);
}

glm::mat4 Camera::GetViewMatrix()
{
	return glm::lookAt(position, position + front, up);
}

glm::mat4 Camera::GetQueryViewMatrix()
{
	return glm::lookAt(pos_query, pos_query + front_query, up_query);
}

glm::mat4 Camera::GetProjection(int guiprojection, float guifov, int screenWidth, int screenHeight)
{
	if (guiprojection == 0) {
		return glm::perspective(glm::radians(guifov), (float)screenWidth / (float)screenHeight, near, far);
	}
	else {
		return glm::ortho(-5.0f, 5.0f, -5.0f, 5.0f, near, far);
	}
}
