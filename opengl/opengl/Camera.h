#ifndef CAMERA_H
#define CAMERA_H


#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glad/glad.h> 
#include <vector>
#include <numeric>

#include <iostream>
#include "FrustumPlane.h"
#include "Shader.h"
#include "SplineBenchmark.h"

enum MOVEMENT {
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT,
	UP,
	DOWN
};

class Camera
{
public:

	// Default attributes
	float yaw = -90.0f;
	float pitch = 0.0f;
	float speed = 2.5f;
	float sens = 0.1f;
	float fov_query = 45.0f;
	float fov_cam = 45.0f;
	float near = 1.0f;
	float far = 100.0f;
	bool moving = true;
	bool detatched = false;

	// Camera Attributes
	glm::vec3 position;
	glm::vec3 front;
	glm::vec3 up;
	glm::vec3 right;
	glm::vec3 worldUp;

	// Query cam attributes
	float yaw_query = -90.0f;
	float pitch_query = 0.0f;
	glm::vec3 pos_query;
	glm::vec3 front_query;
	glm::vec3 up_query;
	glm::vec3 right_query;

	// Array filled with frustum data
	glm::vec4 frustum[6];
	FrustumPlane frustum_planes[6];

	// Frustum drawing variables
	unsigned int fVAO, fVBO;
	std::vector<float> frustumDrawData;
	float heightNear;
	float widthNear;
	float heightFar;
	float widthFar;

	// Spline movement variables
	bool spline_move = false;
	float time_taken = 0.0f;
	float total_time = 0.0f;
	float percentage = 0.0f;
	float lowest_fps = 9999.0f;
	float highest_fps = -9999.0f;
	glm::vec3 pos1;
	glm::vec3 pos2;
	glm::vec3 pos3;
	glm::vec3 pos4;
	SplineBenchmark current_benchmark;

	// Recording variables
	std::vector<SplineBenchmark> benchmarks;
	std::vector<float> averages;

	Camera();
	~Camera();

	void ProcessKeyboard(MOVEMENT direction, float deltaTime);
	void ProcessMouseMovement(float xoffset, float yoffset);
	void calcEulerAngles();
	void stopMoving();
	void startMoving();
	void updateQuery();
	bool aabbInFrustum(glm::vec3 aabbMin, glm::vec3 aabbMax);
	void drawSetup();
	void fillFrustumDrawData();
	void draw(Shader shader);
	void createSplineMovement(std::string name, float cp1[3], float cp2[3], float cp3[3], float cp4[3], float spline_time);
	void updateSplineMovement(float deltaTime, float frameTime);
	glm::vec3 getBezierPointAt(float t);

	glm::mat4 GetViewMatrix();
	glm::mat4 GetQueryViewMatrix();
	glm::mat4 GetProjection(int guiprojection, float guifov,  int screenWidth, int screenHeight);
	
};

#endif

