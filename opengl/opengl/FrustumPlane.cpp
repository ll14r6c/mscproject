#include "FrustumPlane.h"



FrustumPlane::FrustumPlane()
{
}


FrustumPlane::~FrustumPlane()
{
}

void FrustumPlane::set(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3)
{
	// Set point as one of the given points
	point = p2;

	// Calulate the normal for the given triangle, assuming a clockwise order
	glm::vec3 p1subp2 = p1 - p2;
	glm::vec3 p3subp2 = p3 - p2;

	// Normalise the normal and set value
	normal = glm::cross(p3subp2, p1subp2);
	normal = glm::normalize(normal);

	// Set the distance variable 
	dist = -glm::dot(normal, point);
}

float FrustumPlane::distance(glm::vec3 point)
{
	return (dist + glm::dot(normal, point));
}
