#ifndef FRUSTUMPLANE_H
#define FRUSTUMPLANE_H

#include <glm/glm.hpp>

class FrustumPlane
{
public:
	FrustumPlane();
	~FrustumPlane();

	void set(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3);
	float distance(glm::vec3 point);

	glm::vec3 point;
	glm::vec3 normal;
	float dist;
};

#endif