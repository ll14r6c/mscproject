#include "Mesh.h"

Mesh::Mesh(std::string inFilePath, std::string inTexFilePath)
{
	// Load in the given texture
	mytexture = new Texture(inTexFilePath);
	load(inFilePath);

	// Setup draw buffers for mesh and bounding box
	setup();
	storeBoundingBox();
	boundingBoxSetup();
}

Mesh::~Mesh()
{
}

void Mesh::draw(Shader shader)
{
	// Use the given shader
	shader.use();

	// bind texture
	mytexture->bind();

	// draw mesh
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, (GLsizei)concatFileData.size());
}

void Mesh::load(std::string inFilePath)
{
	// Check the file exists
	std::ifstream file(inFilePath.c_str());
	if (!file.good()) {
		std::cerr << "Error: " << inFilePath << " not found\n";
		successfulLoad = false;
		return;
	}

	// While the file still has lines left to parse
	std::string line;
	while (std::getline(file, line)) {
		std::string text;
		std::istringstream iss(line);

		// Check the beginning of each line, then add following data to the correct array
		iss >> text;
		if (text == "v") {
			glm::vec3 vertex;
			iss >> vertex.x;
			iss >> vertex.y;
			iss >> vertex.z;
			temp_vertices.push_back(vertex);
		}

		if (text == "vn") {
			glm::vec3 normal;
			iss >> normal.x;
			iss >> normal.y;
			iss >> normal.z;
			temp_normals.push_back(normal);
		}

		if (text == "vt") {
			glm::vec2 texCoord;
			iss >> texCoord.x;
			iss >> texCoord.y;
			temp_textureCoords.push_back(texCoord);
		}

		if (text == "f") {
			faceCount++;

			std::vector<std::string> tokens;
			std::string s = iss.str();

			// Replace // or / in obj
			//for (int i = 0; i < 6; i++) {
			//	s.replace(s.find("//"), 2, " ");
			//}

			for (int i = 0; i < 6; i++) {
				s.replace(s.find("/"), 1, " ");
			}

			// Split string by ' ' delimiter
			std::stringstream toCheck(s);
			std::string intermediate;
			while (getline(toCheck, intermediate, ' '))
			{
				tokens.push_back(intermediate);
			}
			vIndices.push_back(std::stoi(tokens[1]));
			vIndices.push_back(std::stoi(tokens[4]));
			vIndices.push_back(std::stoi(tokens[7]));

			tIndices.push_back(std::stoi(tokens[2]));
			tIndices.push_back(std::stoi(tokens[5]));
			tIndices.push_back(std::stoi(tokens[8]));

			nIndices.push_back(std::stoi(tokens[3]));
			nIndices.push_back(std::stoi(tokens[6]));
			nIndices.push_back(std::stoi(tokens[9]));

		}

	}

	// For each vertex of each triangle
	for (unsigned int i = 0; i < vIndices.size(); i++) {

		// Get the indices of its attributes
		uint32_t vertexIndex = vIndices[i];
		uint32_t uvIndex = tIndices[i];
		uint32_t normalIndex = nIndices[i];

		// Get the attributes thanks to the index
		glm::vec3 vertex = temp_vertices[vertexIndex - 1];
		glm::vec2 uv = temp_textureCoords[uvIndex - 1];
		glm::vec3 normal = temp_normals[normalIndex - 1];

		// Put the attributes in buffers
		vertices.push_back(vertex);
		textureCoords.push_back(uv);
		normals.push_back(normal);

	}

	// reset bounding box values
	minXYZ = glm::vec3(999.0f, 999.0f, 999.0f);
	maxXYZ = glm::vec3(-999.0f, -999.0f, -999.0f);


	// Add vertices and normals into one big concatenated array
	// E.g. {pos.x, pos.y, pos.z, norm.x, norm.y, norm.z, tex.x, tex.y...}
	for (unsigned int a = 0; a < vertices.size(); a++) {
		concatFileData.push_back(vertices[a].x);
		concatFileData.push_back(vertices[a].y);
		concatFileData.push_back(vertices[a].z);
		concatFileData.push_back(normals[a].x);
		concatFileData.push_back(normals[a].y);
		concatFileData.push_back(normals[a].z);
		concatFileData.push_back(textureCoords[a].x);
		concatFileData.push_back(textureCoords[a].y);

		// Bounding box checks (max)
		if (vertices[a].x > maxXYZ.x) {maxXYZ.x = vertices[a].x;}
		if (vertices[a].y > maxXYZ.y) {maxXYZ.y = vertices[a].y;}
		if (vertices[a].z > maxXYZ.z) {maxXYZ.z = vertices[a].z;}

		// Bounding box checks (min)
		if (vertices[a].x < minXYZ.x) {minXYZ.x = vertices[a].x;}
		if (vertices[a].y < minXYZ.y) {minXYZ.y = vertices[a].y;}
		if (vertices[a].z < minXYZ.z) {minXYZ.z = vertices[a].z;}

	}

	successfulLoad = true;

	// Clear vectors excluding the concatinated vector
	clear();

}

void Mesh::setup()
{
	// create buffers/arrays
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	// load data into vertex buffers
	glBufferData(GL_ARRAY_BUFFER, concatFileData.size() * sizeof(float), concatFileData.data(), GL_STATIC_DRAW);

	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	// normal attribute
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
	// vertex texture coords
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(2);

	// cleanup
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void Mesh::boundingBoxSetup()
{
	glGenVertexArrays(1, &bbVAO);
	glGenBuffers(1, &bbVBO);

	glBindVertexArray(bbVAO);
	glBindBuffer(GL_ARRAY_BUFFER, bbVBO);

	glBufferData(GL_ARRAY_BUFFER, bboxData.size() * sizeof(float), bboxData.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	// cleanup
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

// Clear the vectors
void Mesh::clear()
{
	vIndices.clear();
	nIndices.clear();
	tIndices.clear();

	temp_vertices.clear();
	temp_normals.clear();
	temp_textureCoords.clear();

	vertices.clear();
	normals.clear();
	textureCoords.clear();
}

// Translate the mesh model matrix
void Mesh::translate(glm::vec3 translationVec)
{
	// Translate model vector
	model = glm::translate(model, translationVec);

	// Translate the bounding box min and max
	minXYZ = model * glm::vec4(minXYZ, 1.0f);
	maxXYZ = model * glm::vec4(maxXYZ, 1.0f);
}

// Scale the mesh model matrix
void Mesh::scale(glm::vec3 scaleVec)
{
	model = glm::scale(model, scaleVec);

	// Scale the bounding box min and max
	minXYZ = model * glm::vec4(minXYZ, 1.0f);
	maxXYZ = model * glm::vec4(maxXYZ, 1.0f);
}

void Mesh::drawBoundingBox(Shader shader)
{
	// Use the given shader
	shader.use();

	// bind texture
	mytexture->bind();

	glBindVertexArray(bbVAO);
	glDrawArrays(GL_LINES, 0, bboxData.size());
}

void Mesh::storeBoundingBox()
{
	// store the bounding box vertices
	bboxData = {
		// Verticle lines
	   minXYZ.x, minXYZ.y, minXYZ.z,
	   minXYZ.x, maxXYZ.y, minXYZ.z,
	   minXYZ.x, minXYZ.y, maxXYZ.z,
	   minXYZ.x, maxXYZ.y, maxXYZ.z,
	   maxXYZ.x, minXYZ.y, minXYZ.z,
	   maxXYZ.x, maxXYZ.y, minXYZ.z,
	   maxXYZ.x, minXYZ.y, maxXYZ.z,
	   maxXYZ.x, maxXYZ.y, maxXYZ.z,

	   // Horizontal lines (top)
	   minXYZ.x, maxXYZ.y, minXYZ.z,
	   maxXYZ.x, maxXYZ.y, minXYZ.z,
	   minXYZ.x, maxXYZ.y, minXYZ.z,
	   minXYZ.x, maxXYZ.y, maxXYZ.z,
	   minXYZ.x, maxXYZ.y, maxXYZ.z,
	   maxXYZ.x, maxXYZ.y, maxXYZ.z,
	   maxXYZ.x, maxXYZ.y, maxXYZ.z,
	   maxXYZ.x, maxXYZ.y, minXYZ.z,

	   // Horizontal lines (bottom)
	   minXYZ.x, minXYZ.y, minXYZ.z,
	   minXYZ.x, minXYZ.y, maxXYZ.z,
	   minXYZ.x, minXYZ.y, maxXYZ.z,
	   maxXYZ.x, minXYZ.y, maxXYZ.z,
	   maxXYZ.x, minXYZ.y, maxXYZ.z,
	   maxXYZ.x, minXYZ.y, minXYZ.z,
	   maxXYZ.x, minXYZ.y, minXYZ.z,
	   minXYZ.x, minXYZ.y, minXYZ.z,
	};

	// Bind VBO
	glBindBuffer(GL_ARRAY_BUFFER, bbVBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, bboxData.size() * sizeof(float), bboxData.data());

}

// Replace the current mesh with another mesh
void Mesh::replaceMesh(std::string newFilePath)
{
	// Empty all arrays
	fullClear();

	// load new file
	load(newFilePath);

	// setup arrays with new mesh data
	setup();
	storeBoundingBox();
	boundingBoxSetup();

	minXYZ = model * glm::vec4(minXYZ, 1.0f);
	maxXYZ = model * glm::vec4(maxXYZ, 1.0f);
}

// Replace the current texture with another texture
void Mesh::replaceTexture(std::string newFilePath)
{
	mytexture->replaceTexture(newFilePath);
}

// Clear all vectors including the concat file data
void Mesh::fullClear()
{
	concatFileData.clear();
	clear();
}

// Clear all the vectors and opengl buffers
void Mesh::fullDelete()
{
	// Empty all arrays
	fullClear();

	// delete resources
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);

	glDeleteVertexArrays(1, &bbVAO);
	glDeleteBuffers(1, &bbVBO);
}
