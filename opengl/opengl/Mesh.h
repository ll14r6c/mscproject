#ifndef MESH_H
#define MESH_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <string>
#include <vector>
#include <glad/glad.h> 

#include "Shader.h"
#include "Texture.h"

class Mesh
{
public:
	Mesh(std::string inFilePath, std::string inTexFilePath);
	~Mesh();

	void draw(Shader shader);
	void load(std::string inFilePath);
	void setup();
	void boundingBoxSetup();
	void clear();
	void translate(glm::vec3 translationVec);
	void scale(glm::vec3 scaleVec);
	void drawBoundingBox(Shader shader);
	void storeBoundingBox();
	void replaceMesh(std::string newFilePath);
	void replaceTexture(std::string newFilePath);
	void fullClear();
	void fullDelete();

	std::vector<uint32_t> vIndices;
	std::vector<uint32_t> nIndices;
	std::vector<uint32_t> tIndices;

	std::vector<glm::vec3> temp_vertices;
	std::vector<glm::vec3> temp_normals;
	std::vector<glm::vec2> temp_textureCoords;

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> textureCoords;

	std::vector<float> concatFileData;
	std::vector<float> bboxData;

	// Load texture
	Texture *mytexture;

	// Model matrix
	glm::mat4 model = glm::mat4(1.0f);

	// Bounding box
	glm::vec3 minXYZ = glm::vec3(999.0f, 999.0f, 999.0f);
	glm::vec3 maxXYZ = glm::vec3(-999.0f, -999.0f, -999.0f);

	bool successfulLoad = false;
	int faceCount = 0;

private:
	// Mesh buffers
	unsigned int VAO, VBO;

	// Bounding box buffers
	unsigned int bbVAO, bbVBO;
};

#endif