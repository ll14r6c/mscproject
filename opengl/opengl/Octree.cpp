#include "Octree.h"



Octree::Octree(glm::vec3 inMinPosition, glm::vec3 inMaxPosition)
{
	minPosition = inMinPosition;
	maxPosition = inMaxPosition;

	// top
	topNorthEast = nullptr;
	topNorthWest = nullptr;
	topSouthEast = nullptr;
	topSouthWest = nullptr;
	// bot
	botNorthEast = nullptr;
	botNorthWest = nullptr;
	botSouthEast = nullptr;
	botSouthWest = nullptr;

	setupDrawData();
}

Octree::~Octree()
{
	glDeleteVertexArrays(1, &oVAO);
	glDeleteBuffers(1, &oVBO);
}

void Octree::setupDrawData()
{
	// Set the quad draw data
	octDrawData = {
		// Verticle lines
	   minPosition.x, minPosition.y, minPosition.z,
	   minPosition.x, maxPosition.y, minPosition.z,
	   minPosition.x, minPosition.y, maxPosition.z,
	   minPosition.x, maxPosition.y, maxPosition.z,
	   maxPosition.x, minPosition.y, minPosition.z,
	   maxPosition.x, maxPosition.y, minPosition.z,
	   maxPosition.x, minPosition.y, maxPosition.z,
	   maxPosition.x, maxPosition.y, maxPosition.z,

	   // Horizontal lines (top)
	   minPosition.x, maxPosition.y, minPosition.z,
	   maxPosition.x, maxPosition.y, minPosition.z,
	   minPosition.x, maxPosition.y, minPosition.z,
	   minPosition.x, maxPosition.y, maxPosition.z,
	   minPosition.x, maxPosition.y, maxPosition.z,
	   maxPosition.x, maxPosition.y, maxPosition.z,
	   maxPosition.x, maxPosition.y, maxPosition.z,
	   maxPosition.x, maxPosition.y, minPosition.z,

	   // Horizontal lines (bottom)
	   minPosition.x, minPosition.y, minPosition.z,
	   minPosition.x, minPosition.y, maxPosition.z,
	   minPosition.x, minPosition.y, maxPosition.z,
	   maxPosition.x, minPosition.y, maxPosition.z,
	   maxPosition.x, minPosition.y, maxPosition.z,
	   maxPosition.x, minPosition.y, minPosition.z,
	   maxPosition.x, minPosition.y, minPosition.z,
	   minPosition.x, minPosition.y, minPosition.z,
	};


	glGenVertexArrays(1, &oVAO);
	glGenBuffers(1, &oVBO);

	glBindVertexArray(oVAO);
	glBindBuffer(GL_ARRAY_BUFFER, oVBO);

	glBufferData(GL_ARRAY_BUFFER, octDrawData.size() * sizeof(float), octDrawData.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	// cleanup
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void Octree::insert(Mesh toInsert)
{
	// Check to see if current quad can contain the mesh
	if (!inBoundary(toInsert)) {
		return;
	}

	// Ensure to octant being inserted into is subdivided
	if (!is_subdivided) {
		subdivide();
	}

	if (is_subdivided == true) {
		// Check each child node to see if the mesh object can be contained

		// top
		if (topNorthEast->inBoundary(toInsert)) {
			topNorthEast->insert(toInsert);
		}
		else if (topNorthWest->inBoundary(toInsert)) {
			topNorthWest->insert(toInsert);
		}
		else if (topSouthEast->inBoundary(toInsert)) {
			topSouthEast->insert(toInsert);
		}
		else if (topSouthWest->inBoundary(toInsert)) {
			topSouthWest->insert(toInsert);
		}
		
		// bot
		else if (botNorthEast->inBoundary(toInsert)) {
			botNorthEast->insert(toInsert);
		}
		else if (botNorthWest->inBoundary(toInsert)) {
			botNorthWest->insert(toInsert);
		}
		else if (botSouthEast->inBoundary(toInsert)) {
			botSouthEast->insert(toInsert);
		}
		else if (botSouthWest->inBoundary(toInsert)) {
			botSouthWest->insert(toInsert);
		}
		// mesh cannot be stored in children so store in parent
		else {
			store(toInsert);
		}
	}
	else {
		// Add to current node
		store(toInsert);
	}
}

bool Octree::inBoundary(Mesh &mesh)
{
	// If the mesh bounding box is within the quad bounds return true
	if (mesh.minXYZ.x > minPosition.x &&
		mesh.minXYZ.y > minPosition.y &&
		mesh.minXYZ.z > minPosition.z &&
		mesh.maxXYZ.x < maxPosition.x &&
		mesh.maxXYZ.y < maxPosition.y &&
		mesh.maxXYZ.z < maxPosition.z) {
		return true;
	}

	return false;
}

void Octree::drawNode()
{
	// draw mesh
	glBindVertexArray(oVAO);
	glDrawArrays(GL_LINES, 0, octDrawData.size());
}

void Octree::draw()
{
	drawNode();
	if (is_subdivided) {
		// top
		topNorthEast->draw();
		topNorthWest->draw();
		topSouthEast->draw();
		topSouthWest->draw();

		// bot
		botNorthEast->draw();
		botNorthWest->draw();
		botSouthEast->draw();
		botSouthWest->draw();
	}
}

void Octree::drawNodeMesh(Camera &cam, Shader &shader, bool draw_bounding, std::vector<Mesh> &list)
{
	// If the frustum intersects with the mesh in the current quad
	for (Mesh &m : list) {
		if (cam.aabbInFrustum(m.minXYZ, m.maxXYZ)) {
			shader.setMat4("model", m.model);
			m.draw(shader);
			if (draw_bounding) {
				m.drawBoundingBox(shader);
			}
		}
	}
}

void Octree::store(Mesh & toStore)
{
	meshList.push_back(toStore);
}

void Octree::subdivide()
{
	glm::vec3 midPoint = (maxPosition + minPosition) * 0.5f;

	// top
	topNorthEast = new Octree(glm::vec3((minPosition.x + maxPosition.x) * 0.5f, (minPosition.y + maxPosition.y) * 0.5f, minPosition.z), glm::vec3(maxPosition.x, maxPosition.y, (minPosition.z + maxPosition.z) * 0.5f));
	topNorthWest = new Octree(glm::vec3(minPosition.x, (minPosition.y + maxPosition.y) * 0.5f, minPosition.z), glm::vec3((minPosition.x + maxPosition.x) * 0.5f, maxPosition.y, (minPosition.z + maxPosition.z) * 0.5f));
	topSouthEast = new Octree(midPoint, maxPosition);
	topSouthWest = new Octree(glm::vec3(minPosition.x, (minPosition.y + maxPosition.y) * 0.5f, (minPosition.x + maxPosition.x) * 0.5f), glm::vec3((minPosition.x + maxPosition.x) * 0.5f, maxPosition.y, maxPosition.z));

	// bot
	botNorthEast = new Octree(glm::vec3((minPosition.x + maxPosition.x) * 0.5f, minPosition.y, minPosition.z), glm::vec3(maxPosition.x, (minPosition.y + maxPosition.y) * 0.5f, (minPosition.z + maxPosition.z) * 0.5f));
	botNorthWest = new Octree(minPosition, midPoint);
	botSouthEast = new Octree(glm::vec3((minPosition.x + maxPosition.x) * 0.5f, minPosition.y, (minPosition.z + maxPosition.z) * 0.5f), glm::vec3(maxPosition.x, (minPosition.y + maxPosition.y) * 0.5f, maxPosition.z));
	botSouthWest = new Octree(glm::vec3(minPosition.x, minPosition.y, (minPosition.z + maxPosition.z) * 0.5f), glm::vec3((minPosition.x + maxPosition.x) * 0.5f, (minPosition.y + maxPosition.y) * 0.5f, maxPosition.z));

	is_subdivided = true;
}

// Method to query the octree with a given camera and draw the meshes inside of that
void Octree::queryAndDraw(Camera & cam, Shader shader, bool draw_bounding)
{
	drawNodeMesh(cam, shader, draw_bounding, meshList);
	if (is_subdivided) {
		// If the frustum intersects with the top childs bounds
		if (cam.aabbInFrustum(topNorthEast->minPosition, topNorthEast->maxPosition) == true) {
			topNorthEast->queryAndDraw(cam, shader, draw_bounding);
		}
		if (cam.aabbInFrustum(topNorthWest->minPosition, topNorthWest->maxPosition) == true) {
			topNorthWest->queryAndDraw(cam, shader, draw_bounding);
		}
		if (cam.aabbInFrustum(topSouthEast->minPosition, topSouthEast->maxPosition) == true) {
			topSouthEast->queryAndDraw(cam, shader, draw_bounding);
		}
		if (cam.aabbInFrustum(topSouthWest->minPosition, topSouthWest->maxPosition) == true) {
			topSouthWest->queryAndDraw(cam, shader, draw_bounding);
		}

		// If the frustum intersects with the bot childs bounds
		if (cam.aabbInFrustum(botNorthEast->minPosition, botNorthEast->maxPosition) == true) {
			botNorthEast->queryAndDraw(cam, shader, draw_bounding);
		}
		if (cam.aabbInFrustum(botNorthWest->minPosition, botNorthWest->maxPosition) == true) {
			botNorthWest->queryAndDraw(cam, shader, draw_bounding);
		}
		if (cam.aabbInFrustum(botSouthEast->minPosition, botSouthEast->maxPosition) == true) {
			botSouthEast->queryAndDraw(cam, shader, draw_bounding);
		}
		if (cam.aabbInFrustum(botSouthWest->minPosition, botSouthWest->maxPosition) == true) {
			botSouthWest->queryAndDraw(cam, shader, draw_bounding);
		}
	}
}

void Octree::deleteTree(Octree * oct)
{
	// Case of non seperated octant
	if (oct == nullptr) {
		return;
	}

	// Clear the meshList for the current node
	meshList.clear();

	if (is_subdivided) {
		// Clear all child trees
		deleteTree(topNorthEast);
		deleteTree(topNorthWest);
		deleteTree(topSouthEast);
		deleteTree(topSouthWest);

		deleteTree(botNorthEast);
		deleteTree(botNorthWest);
		deleteTree(botSouthEast);
		deleteTree(botSouthWest);

		// Delete all child trees
		delete topNorthEast;
		delete topNorthWest;
		delete topSouthEast;
		delete topSouthWest;

		delete botNorthEast;
		delete botNorthWest;
		delete botSouthEast;
		delete botSouthWest;
	}
}
