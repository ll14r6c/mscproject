#ifndef OCTREE_H
#define OCTREE_H

#include <glm/glm.hpp>
#include <vector>
#include <glad/glad.h>

#include "Mesh.h"
#include "Camera.h"
#include "Shader.h"

class Octree
{
public:
	Octree(glm::vec3 inMinPosition, glm::vec3 inMaxPosition);
	~Octree();

	// Child trees
	// top
	Octree *topNorthEast;
	Octree *topNorthWest;
	Octree *topSouthEast;
	Octree *topSouthWest;

	// bot
	Octree *botNorthEast;
	Octree *botNorthWest;
	Octree *botSouthEast;
	Octree *botSouthWest;

	glm::vec3 minPosition;
	glm::vec3 maxPosition;

	std::vector<Mesh> meshList;
	bool is_subdivided = false;

	// Drawing variables
	unsigned int oVAO, oVBO;
	std::vector<float> octDrawData;

	void setupDrawData();
	void insert(Mesh toInsert);
	bool inBoundary(Mesh &mesh);
	void drawNode();
	void draw();
	void drawNodeMesh(Camera &cam, Shader &shader, bool draw_bounding, std::vector<Mesh> &list);
	void store(Mesh &toStore);
	void subdivide();
	void queryAndDraw(Camera &cam, Shader shader, bool draw_bounding);
	void deleteTree(Octree* oct);
};

#endif