#include "Quadtree.h"


Quadtree::Quadtree(glm::vec3 inMinPosition, glm::vec3 inMaxPosition)
{
	minPosition = inMinPosition;
	maxPosition = inMaxPosition;

	topLeft  = nullptr;
	topRight = nullptr;
	botLeft  = nullptr;
	botRight = nullptr;

	setupDrawData();
}

Quadtree::~Quadtree()
{
	glDeleteVertexArrays(1, &qVAO);
	glDeleteBuffers(1, &qVBO);
}

void Quadtree::setupDrawData()
{
	// Set the Y values to 0
	minPosition.y = 0;
	maxPosition.y = 0;

	// Set the quad draw data
	quadDrawData = {
		minPosition.x, minPosition.y, minPosition.z,
		minPosition.x, minPosition.y, maxPosition.z,

		minPosition.x, minPosition.y, maxPosition.z,
		maxPosition.x, minPosition.y, maxPosition.z,

		maxPosition.x, minPosition.y, maxPosition.z,
		maxPosition.x, minPosition.y, minPosition.z,

		maxPosition.x, minPosition.y, minPosition.z,
		minPosition.x, minPosition.y, minPosition.z,
	};

	glGenVertexArrays(1, &qVAO);
	glGenBuffers(1, &qVBO);

	glBindVertexArray(qVAO);
	glBindBuffer(GL_ARRAY_BUFFER, qVBO);

	glBufferData(GL_ARRAY_BUFFER, quadDrawData.size() * sizeof(float), quadDrawData.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	// cleanup
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void Quadtree::insert(Mesh &toInsert)
{
	// Check to see if current quad can contain the mesh
	if (!inBoundary(toInsert)) {
		return;
	}

	// Ensure to quad being imserted is subdivided
	if (!is_subdivided) {
		subdivide();
	}

	if (is_subdivided == true) {

		// Check each child node to see if the mesh object can be contained
		if (topLeft->inBoundary(toInsert)) {
			topLeft->insert(toInsert);
		}
		else if (topRight->inBoundary(toInsert)) {
			topRight->insert(toInsert);
		}
		else if (botLeft->inBoundary(toInsert)) {
			botLeft->insert(toInsert);
		}
		else if (botRight->inBoundary(toInsert)) {
			botRight->insert(toInsert);
		}
		// mesh cannot be stored in children so store in parent
		else {
			store(toInsert);
		}
	}
	else {

		std::cout << "Else" << std::endl;
		// Add to current node
		store(toInsert);
	}
}

void Quadtree::deleteTree(Quadtree* quad)
{

	// Case of non seperated quads
	if (quad == nullptr) {
		return;
	}

	// Clear the meshList for the current node
	for (auto &m : meshList) {
		m.fullDelete();
	}

	if (is_subdivided) {

		// Clear all child trees
		deleteTree(topLeft);
		deleteTree(topRight);
		deleteTree(botLeft);
		deleteTree(botRight);

		// Delete all child trees
		free(quad);
	}
}

void Quadtree::drawNode()
{
	// draw mesh
	glBindVertexArray(qVAO);
	glDrawArrays(GL_LINES, 0, quadDrawData.size());
}

void Quadtree::draw()
{
	drawNode();
	if (is_subdivided) {
		topLeft->draw();
		topRight->draw();
		botLeft->draw();
		botRight->draw();
	}
}

bool Quadtree::inBoundary(Mesh &mesh)
{
	// If the mesh bounding box is within the quad bounds return true
	if (mesh.minXYZ.x > minPosition.x &&
		mesh.minXYZ.z > minPosition.z &&
		mesh.maxXYZ.x < maxPosition.x &&
		mesh.maxXYZ.z < maxPosition.z) {
		return true;
	}

	return false;
}

void Quadtree::store(Mesh & toStore)
{
	meshList.push_back(toStore);
}

void Quadtree::subdivide()
{
	glm::vec3 midPoint = (maxPosition + minPosition) * 0.5f;
	topRight = new Quadtree(glm::vec3((minPosition.x + maxPosition.x) * 0.5f, 0.0f, minPosition.z), glm::vec3(maxPosition.x, 0.0f, (minPosition.z + maxPosition.z) * 0.5f));
	botLeft  = new Quadtree(glm::vec3(minPosition.x, 0.0f, (minPosition.z + maxPosition.z) * 0.5f), glm::vec3((minPosition.x + maxPosition.x) * 0.5f, 0.0f, maxPosition.z));
	botRight = new Quadtree(midPoint, maxPosition);
	topLeft  = new Quadtree(minPosition, midPoint);


	is_subdivided = true;
}

// Method to query the quadtree with a given camera and draw the meshes inside of that
void Quadtree::queryAndDraw(Camera & cam, Shader shader, bool draw_bounding)
{
	if (is_subdivided) {
		// If the frustum intersects with the childs bounds
		if (cam.aabbInFrustum(topRight->minPosition, topRight->maxPosition) == true) {
			topRight->queryAndDraw(cam, shader, draw_bounding);
		}
		if (cam.aabbInFrustum(botLeft->minPosition, botLeft->maxPosition) == true) {
			botLeft->queryAndDraw(cam, shader, draw_bounding);
		}
		if (cam.aabbInFrustum(botRight->minPosition, botRight->maxPosition) == true) {
			botRight->queryAndDraw(cam, shader, draw_bounding);
		}
		if (cam.aabbInFrustum(topLeft->minPosition, topLeft->maxPosition) == true) {
			topLeft->queryAndDraw(cam, shader, draw_bounding);
		}
	}

	// If the frustum intersects with the mesh in the current quad
	for (Mesh &m : meshList) {
		if (cam.aabbInFrustum(m.minXYZ, m.maxXYZ)) {
			shader.setMat4("model", m.model);
			m.draw(shader);
			if (draw_bounding) {
				m.drawBoundingBox(shader);
			}
		}
	}
}
