#ifndef QUADTREE_H
#define QUADTREE_H

#include <glm/glm.hpp>
#include <vector>
#include <glad/glad.h> 

#include "Mesh.h"
#include "Camera.h"
#include "Shader.h"

class Quadtree
{
public:
	Quadtree(glm::vec3 inMinPosition, glm::vec3 inMaxPosition);
	~Quadtree();

	// Child trees
	Quadtree *topLeft;
	Quadtree *topRight;
	Quadtree *botLeft;
	Quadtree *botRight;

	glm::vec3 minPosition;
	glm::vec3 maxPosition;

	std::vector<Mesh> meshList;
	bool is_subdivided = false;

	// Drawing variables
	unsigned int qVAO, qVBO;
	std::vector<float> quadDrawData;

	void setupDrawData();
	void insert(Mesh &toInsert);
	void deleteTree(Quadtree* quad);
	void drawNode();
	void draw();
	bool inBoundary(Mesh &mesh);
	void store(Mesh &toStore);
	void subdivide();
	void queryAndDraw(Camera &cam, Shader shader, bool draw_bounding);
};

#endif