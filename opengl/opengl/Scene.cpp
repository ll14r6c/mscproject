#include "Scene.h"



Scene::Scene()
{
}


Scene::~Scene()
{
}

void Scene::clearStructures()
{
	for (auto &m : models) {
		m.fullDelete();
	}
	models.clear();
	//qTree->deleteTree(qTree);
	//oTree->deleteTree(oTree);
}

void Scene::createOrderedScene(int spacing, int width, glm::vec3 startPosition, std::string model_path)
{
	clearStructures();
	qTree = new Quadtree(startPosition - 10.0f, startPosition + glm::vec3((float)(width*spacing)) + 10.0f);
	oTree = new Octree(startPosition - 10.0f, startPosition + glm::vec3((float)(width*spacing)) + 10.0f);

	float percentChange = 1.0 / spacing * width / spacing * width / spacing * width;

	totalTriangles = 0;
	percentage = 0.0f;
	for (float x = startPosition.x; x < startPosition.x + (spacing*width); x += spacing) {
		for (float y = startPosition.y; y < startPosition.y + (spacing*width); y += spacing) {
			for (float z = startPosition.z; z < startPosition.z + (spacing*width); z += spacing) {
				Mesh temp(model_path, "textures/light.png");
				temp.translate(glm::vec3(x, y, z));

				// Insert into data structure(s)
				// Linear
				models.push_back(temp);
				// Quadtree
				qTree->insert(temp);
				// Octree
				oTree->insert(temp);

				// Add model triangle count to total scene triangle count
				totalTriangles += temp.faceCount;

				// Calculate the percentage
				percentage += percentChange;
			}
		}
	}
}

void Scene::createRandomScene(int seed, glm::vec3 minPosition, glm::vec3 maxPosition, int totalModels)
{
	clearStructures();
	qTree = new Quadtree(minPosition - 10.0f, maxPosition + 10.0f);
	oTree = new Octree(minPosition - 20.0f, maxPosition + 20.0f);

	percentage = 0.0f;
	float percentChange = 1.0 / totalModels;

	srand(seed);

	for (int i = 0; i < totalModels; i++) {
		Mesh temp(getRandomModel(), getRandomTexture());
		temp.scale(glm::vec3(random(0.1, 4.0)));
		temp.translate(glm::vec3(random(minPosition.x, maxPosition.x), random(minPosition.y, maxPosition.y), random(minPosition.z, maxPosition.z)));

		// Insert into data structure(s)
		// Linear
		models.push_back(temp);
		// Quadtree
		qTree->insert(temp);
		// Octree
		oTree->insert(temp);

		// Add model triangle count to total scene triangle count
		totalTriangles += temp.faceCount;

		// Calculate the percentage
		percentage += percentChange;

	}
}

void Scene::createCityScene(int seed, int width, int roadVal, glm::vec3 startPosition)
{
	clearStructures();
	qTree = new Quadtree(startPosition - 1000.0f, startPosition + glm::vec3(1999));
	oTree = new Octree(startPosition - 1000.0f, startPosition + glm::vec3(1999));

	int size = width;

	float percentChange = 1.0 / (size / 2) / (size / 2);
	totalTriangles = 0;
	percentage = 0.0f;

	srand(seed);
	for (int x = 0; x < size; x += 2) {
		for (int y = 0; y < size; y += 2) {

			Mesh temp("models/cube.obj", "textures/road.jpg");

			if (x % roadVal == 0 || y % roadVal == 0) {
				temp.mytexture = road;
				temp.scale(glm::vec3(1.0f, 0.1f, 1.0f));
				temp.translate(glm::vec3(x, 0.0f, y));
			}
			else {
				float randomVal = (rand() % 5);
				if (randomVal < 0.75f) {
					temp.mytexture = grass;
					temp.scale(glm::vec3(1.0f, 0.15f, 1.0f));
				}
				else {
					temp.mytexture = building;
					temp.scale(glm::vec3(1.0f, randomVal, 1.0f));
				}
				temp.translate(glm::vec3(x, -randomVal * 0.1f, y));
			}

			// Insert into data structure(s)
			// Linear
			models.push_back(temp);
			// Quadtree
			qTree->insert(temp);
			// Octree
			oTree->insert(temp);

			// Add model triangle count to total scene triangle count
			totalTriangles += temp.faceCount;

			// Calculate the percentage
			percentage += percentChange;
		}
	}
}

// Get a random model from a predefined list of models
std::string Scene::getRandomModel()
{
	std::vector<std::string> filenames;
	filenames = {
		"models/sphere.obj",
		"models/monkey.obj",
		"models/cube.obj",
		"models/cone.obj",
		"models/torus.obj",
		"models/cylinder.obj",
		"models/icosphere.obj",
	};
	return filenames[(int)random(0, 6)];
}

// Get a random texture from a predefined list of textures
std::string Scene::getRandomTexture()
{
	std::vector<std::string> filenames;
	filenames = {
		"textures/road.jpg",
		"textures/building.jpg",
		"textures/grass.jpg",
		"textures/light.png",
		"textures/wall.jpg",
	};
	return filenames[(int)random(0, 4)];
}

// Return a random number between a min and max number
float Scene::random(float min, float max)
{
	return min + (rand() % static_cast<int>(max - min + 1));
}
