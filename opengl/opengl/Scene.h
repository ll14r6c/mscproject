#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/random.hpp>
#include <iostream>

#include "Mesh.h"
#include "Quadtree.h"
#include "Octree.h"

class Scene
{
public:
	Scene();
	~Scene();

	void clearStructures();
	void createOrderedScene(int spacing, int width, glm::vec3 startPosition, std::string model_path);
	void createRandomScene(int seed, glm::vec3 minPosition, glm::vec3 maxPosition, int totalModels);
	void createCityScene(int seed, int width, int roadVal, glm::vec3 startPosition);

	std::vector<Mesh> models;
	Quadtree *qTree;
	Octree *oTree;

	float percentage = 1.0f;
	int totalTriangles;

	// Textures for city
	Texture *grass = new Texture("textures/grass.jpg");
	Texture *road = new Texture("textures/road.jpg");
	Texture *building = new Texture("textures/building.jpg");

private:
	std::string getRandomModel();
	std::string getRandomTexture();

	float random(float min, float max);
};

#endif
