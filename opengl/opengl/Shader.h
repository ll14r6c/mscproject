#ifndef SHADER_H
#define SHADER_H

/*
SHADER CLASS CODE TAKEN FROM https://learnopengl.com TUTORIAL
by Joey de Vries https://twitter.com/JoeyDeVriez.
Licensed under the terms of the CC BY-NC 4.0 license as published by Creative Commons https://creativecommons.org/licenses/by/4.0/legalcode
*/


#include <glad/glad.h> // include glad to get all the required OpenGL headers
#include <glm/glm.hpp>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>


class Shader
{
public:
	// the program ID
	unsigned int ID;

	// constructor reads and builds the shader
	Shader(const GLchar* vertexPath, const GLchar* fragmentPath);
	// use/activate the shader
	void use();
	// utility uniform functions
	void setBool(const std::string &name, bool value) const;
	void setInt(const std::string &name, int value) const;
	void setFloat(const std::string &name, float value) const;
	void setVec3(const std::string &name, float x, float y, float z) const;
	void setMat4(const std::string &name, const glm::mat4 &mat) const;

private:
	void checkCompileErrors(unsigned int shader, std::string type);
};

#endif