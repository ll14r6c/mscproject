#include <iostream>
#include <glad/glad.h> 

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GLFW/glfw3.h>

#include "Shader.h"
#include "gui.h"
#include "Camera.h"
#include "Mesh.h"
#include "Texture.h"
#include "Scene.h"

#include <omp.h>

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void processInput(GLFWwindow *window, GUI *mygui);

// settings
const unsigned int SCR_WIDTH = 1280;
const unsigned int SCR_HEIGHT = 720;

// camera
Camera cam;
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

// timing
float deltaTime = 0.0f;	// time between current frame and last frame
float lastFrame = 0.0f;

// Light position
float lightPos[3] = { -1.0f, 0.0f, 0.0f };

int main()
{
	// Instantiate the GLFW window
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_SAMPLES, 4);
	// Create the window object
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "MSc Project", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);

	// let GLFW to capture mouse inputs
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);


	// load all glad OpenGL function pointers
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	cam.drawSetup();
	
	// Enable depth test
	glEnable(GL_DEPTH_TEST);

	// Cull back faces
	glCullFace(GL_BACK);

	// Line with settings
	glLineWidth(3.0f);

	// Enable multisampling
	glEnable(GL_MULTISAMPLE);

	// Enable debug output
	glEnable(GL_DEBUG_OUTPUT);

	// build and compile shaders
	Shader lightingShader("shaders/light_shader.vs", "shaders/light_shader.fs");
	Shader lampShader("shaders/lamp_shader.vs", "shaders/lamp_shader.fs");

	Mesh lightbulb("models/cube.obj", "textures/light.png");

	Scene myScene;

	// Create the imgui context
	GUI *mygui = new GUI(window, &cam, &myScene);

	// render loop
	while (!glfwWindowShouldClose(window))
	{
		// Time logic
		float currentFrame = (float)glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// process all inputs to window
		processInput(window, mygui);

		cam.updateSplineMovement(deltaTime, mygui->getFrameTime());
		cam.fillFrustumDrawData();

		lightPos[0] = mygui->lightPos[0];
		lightPos[1] = mygui->lightPos[1];
		lightPos[2] = mygui->lightPos[2];

		// Start the Dear ImGui frame
		mygui->newFrame();

		// render code
		glClearColor(0.25f, 0.25f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		lightingShader.use();
		lightingShader.setInt("texture1", 0);
		lightingShader.setVec3("lightColor", 1.0f, 1.0f, 1.0f);
		lightingShader.setVec3("lightPos", lightPos[0], lightPos[1], lightPos[2]);
		lightingShader.setVec3("viewPos", cam.pos_query.x, cam.pos_query.y, cam.pos_query.z);

		// pass projection matrix to shader
		glm::mat4 projection = cam.GetProjection(mygui->getProjection(), cam.fov_cam, SCR_WIDTH, SCR_HEIGHT);
		lightingShader.setMat4("projection", projection);

		// camera/view transformation
		glm::mat4 view = cam.GetViewMatrix();
		lightingShader.setMat4("view", view);

		glm::mat4 model = glm::mat4(1.0f);

		// Query variables
		glm::mat4 query_projection = cam.GetProjection(mygui->getProjection(), cam.fov_query, SCR_WIDTH, SCR_HEIGHT);
		glm::mat4 query_view = cam.GetQueryViewMatrix();

		// No Culling
		if (mygui->culling_method == 0) {
			for (Mesh &m : myScene.models) {
				lightingShader.setMat4("model", m.model);
				m.draw(lightingShader);
				if (mygui->boundingBox) {
					m.drawBoundingBox(lightingShader);
				}
			}
		}
		// Linear Culling
		else if (mygui->culling_method == 1) {
			for (Mesh &m : myScene.models) {
				if (cam.aabbInFrustum(m.minXYZ, m.maxXYZ) == true) {
					lightingShader.setMat4("model", m.model);
					m.draw(lightingShader);
					if (mygui->boundingBox) {
						m.drawBoundingBox(lightingShader);
					}
				}
			}
		}
		// Multi-threaded Culling
		else if (mygui->culling_method == 2) {
			#pragma omp parallel for schedule(dynamic)
			for (int i = 0; i < myScene.models.size(); i++) {
				if (cam.aabbInFrustum(myScene.models[i].minXYZ, myScene.models[i].maxXYZ) == true) {
					lightingShader.setMat4("model", myScene.models[i].model);
					myScene.models[i].draw(lightingShader);
					if (mygui->boundingBox) {
						myScene.models[i].drawBoundingBox(lightingShader);
					}
				}
			}
		}
		// Quadtree Culling
		else if (mygui->culling_method == 3) {
			myScene.qTree->queryAndDraw(cam, lightingShader, mygui->boundingBox);
		}
		// Octree Culling
		else if (mygui->culling_method == 4) {
			myScene.oTree->queryAndDraw(cam, lightingShader, mygui->boundingBox);
		}


		// Draw frustum if box is ticked
		if (mygui->draw_frustum) {
			lightingShader.setMat4("model", glm::mat4(1.0f));
			cam.draw(lightingShader);
		}
		// Draw quadtree if box is ticked
		if (mygui->draw_quadtree) {
			lightingShader.setMat4("model", glm::mat4(1.0f));
			myScene.qTree->draw();
		}
		// Draw octree if box is ticked
		if (mygui->draw_octree) {
			lightingShader.setMat4("model", glm::mat4(1.0f));
			myScene.oTree->draw();
		}

		// also draw the lamp object as a cube
		lampShader.use();
		lampShader.setInt("texture1", 0);
		lampShader.setMat4("projection", projection);
		lampShader.setMat4("view", view);
		lightbulb.model = glm::mat4(1.0f);
		lightbulb.translate(glm::vec3(lightPos[0], lightPos[1], lightPos[2]));
		lightbulb.scale(glm::vec3(0.25f));
		lampShader.setMat4("model", lightbulb.model);
		lightbulb.draw(lampShader);

		// Render gui interface
		mygui->render();


		// glfw swap buffers and poll IO events
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// terminate glfw
	glfwTerminate();
	return 0;
}

// process all input
void processInput(GLFWwindow *window, GUI *mygui)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, true);
	}

	// FPS controls
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
		cam.ProcessKeyboard(FORWARD, deltaTime);
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
		cam.ProcessKeyboard(BACKWARD, deltaTime);
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
		cam.ProcessKeyboard(LEFT, deltaTime);
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
		cam.ProcessKeyboard(RIGHT, deltaTime);
	}
	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
		cam.ProcessKeyboard(UP, deltaTime);
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
		cam.ProcessKeyboard(DOWN, deltaTime);
	}

	// User interface controls/In-engine camera toggle
	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) {
		cam.startMoving();
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	}
	if (glfwGetKey(window, GLFW_KEY_TAB) == GLFW_PRESS) {
		cam.stopMoving();
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}

	// GUI Overlay toggle (glfw get key causes lots of toggles, so only toggle once)
	bool isOverlayCurrentlyVisable = glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS;
	if (!mygui->isOverlayVisiable && isOverlayCurrentlyVisable) {
		mygui->toggleOverlay();
	}
	mygui->isOverlayVisiable = isOverlayCurrentlyVisable;

	mygui->cam->updateQuery();
}

// whenever the window size changed, this callback function executes
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void mouse_callback(GLFWwindow * window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = (float)xpos;
		lastY = (float)ypos;
		firstMouse = false;
	}

	float xoffset = (float)xpos - lastX;
	float yoffset = lastY - (float)ypos;

	lastX = (float)xpos;
	lastY = (float)ypos;

	cam.ProcessMouseMovement(xoffset, yoffset);
}
