#ifndef SPLINEBENCHMARK_H
#define SPLINEBENCHMARK_H

#include <string>

class SplineBenchmark
{
public:
	SplineBenchmark(std::string name, float total_time);
	SplineBenchmark();
	~SplineBenchmark();

	std::string name;
	float min;
	float max;
	float average;
};

#endif