#ifndef TEXTURE_H
#define TEXTURE_H

#define STB_IMAGE_STATIC
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <string>
#include <glad/glad.h> 
#include <iostream>
#include <filesystem>

class Texture
{
public:
	Texture(std::string path);
	~Texture();

	void create();
	void bind();
	void replaceTexture(std::string newFilePath);

	unsigned int id;
	std::string path;
};

#endif