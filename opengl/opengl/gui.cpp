#include "gui.h"


GUI::GUI(GLFWwindow* window, Camera *input_cam, Scene *myScene)
{
	cam = input_cam;
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGui::StyleColorsDark();
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init("#version 330");

	theScene = myScene;
}


GUI::~GUI()
{
	// imgui cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
}

// Called each frame
void GUI::newFrame()
{
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();

	showMenu();
	applyCheckboxes();

	if (displayOverlay) {
		showOverlay();
	}
}

// Draw the Imgui interface
void GUI::render()
{
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

}

// Toggle for real time stats on bottom right of the screen
void GUI::toggleOverlay()
{
	displayOverlay = !displayOverlay;
}

// Show the real time stats on the bottom of the screen
void GUI::showOverlay()
{
	ImGuiIO& io = ImGui::GetIO();
	if (staticCorner != -1)
	{
		ImVec2 window_pos = ImVec2((staticCorner & 1) ? io.DisplaySize.x - staticDistance : staticDistance, (staticCorner & 2) ? io.DisplaySize.y - staticDistance : staticDistance);
		ImVec2 window_pos_pivot = ImVec2((staticCorner & 1) ? 1.0f : 0.0f, (staticCorner & 2) ? 1.0f : 0.0f);
		ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
	}
	ImGui::SetNextWindowBgAlpha(0.30f); // Transparent background
	if (ImGui::Begin("Rendering Stats", NULL, (staticCorner != -1 ? ImGuiWindowFlags_NoMove : 0) | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav))
	{
		ImGui::Text("Rendering Stats");
		ImGui::Separator();
		ImGui::Text("Position: %.3f\t%.3f\t%.3f", cam->position.x, cam->position.y, cam->position.z);
		ImGui::Text("Orientation: %.3f", cam->yaw);
		ImGui::Separator();
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		ImGui::Text("Total triangle count: %d", theScene->totalTriangles);
	}
	ImGui::End();
}

// Show the whole Imgui menu created for this application
void GUI::showMenu()
{
	ImGui::Begin("Options Menu", NULL);

	// Debug settings
	if (ImGui::CollapsingHeader("Debug Settings"))
	{
		ImGui::SliderFloat3("Light Position", lightPos, -50.0f, 50.0f);
		ImGui::Checkbox("Detatch camera", &cam->detatched);
		ImGui::Checkbox("Draw Frustum", &draw_frustum);
		ImGui::Checkbox("Draw Bounding Boxes", &boundingBox);
		ImGui::Checkbox("Draw Quadtree", &draw_quadtree);
		ImGui::Checkbox("Draw Octree", &draw_octree);
		ImGui::Checkbox("Wireframe", &wireframe);
		ImGui::Checkbox("Vsync", &vsync);
		ImGui::Checkbox("Backface Culling", &backface_cull);
		ImGui::SameLine();
		ImGui::RadioButton("CW", &polygon_orientation, 0);
		ImGui::SameLine();
		ImGui::RadioButton("CCW", &polygon_orientation, 1);
	}
	ImGui::Spacing();

	// Optimization settings
	if (ImGui::CollapsingHeader("Optimization Settings"))
	{
		ImGui::RadioButton("No Frustum culling", &culling_method, 0);
		ImGui::RadioButton("Linear Frustum culling", &culling_method, 1);
		ImGui::RadioButton("Multi-threaded Frustum culling", &culling_method, 2);
		ImGui::RadioButton("Quadtree Frustum culling", &culling_method, 3);
		ImGui::RadioButton("Octree Frustum culling", &culling_method, 4);
	}
	ImGui::Spacing();

	// Scene generation settings
	if (ImGui::CollapsingHeader("Scene Generations"))
	{
		ImGui::RadioButton("Ordered Scene", &scene_method, 0);
		ImGui::RadioButton("Random Scene", &scene_method, 1);
		ImGui::RadioButton("Simple Cityscape", &scene_method, 2);
		ImGui::Separator();
		ImGui::Spacing();
		// Ordered scene input gui
		if (scene_method == 0) {
			ImGui::InputInt("Space between models", &ordered_spacing);
			ImGui::InputInt("Models per row/column", &ordered_width);
			ImGui::InputFloat3("Starting Position", ordered_start);
			ImGui::BeginChild("Models picker", ImVec2(0.0f, 0.0f), true);
				std::string path = "../opengl/models";
				int ordered_counter = 0;
				for (const auto & entry : std::experimental::filesystem::directory_iterator(path)){
					if (ImGui::Selectable(entry.path().string().c_str(), ordered_model_selected == ordered_counter)) {
						ordered_model_selected = ordered_counter;
						ordered_mesh = entry.path().string();
					}
					ordered_counter++;
				}
			ImGui::EndChild();
		}
		// Random scene input gui
		else if (scene_method == 1) {
			ImGui::InputInt("Random Seed", &random_seed);
			ImGui::InputFloat3("Minimum Position", random_min_position);
			ImGui::InputFloat3("Maximum Position", random_max_position);
			ImGui::InputInt("Total models", &random_total);
		}
		// Simple city scene input
		else if (scene_method == 2) {
			ImGui::InputInt("City Seed", &city_seed);
			ImGui::InputFloat3("Starting Position", city_position);
			ImGui::InputInt("Road Value", &city_road);
			ImGui::InputInt("Models per row", &city_width);
		}
		ImGui::Separator();
		ImGui::Spacing();
		if (ImGui::Button("Generate"))
		{
			// Generate ordered scene
			if (scene_method == 0) {
				theScene->createOrderedScene(ordered_spacing, ordered_width, glm::vec3(ordered_start[0], ordered_start[1], ordered_start[2]), ordered_mesh);
			}
			// Generate random scene
			else if (scene_method == 1) {
				theScene->createRandomScene(random_seed, glm::vec3(random_min_position[0], random_min_position[1], random_min_position[2]), glm::vec3(random_max_position[0], random_max_position[1], random_max_position[2]), random_total);
			}
			// Generate city scene
			else if (scene_method == 2) {
				theScene->createCityScene(city_seed, city_width, city_road, glm::vec3(city_position[0], city_position[1], city_position[2]));
			}
		}
		ImGui::SameLine();
		ImGui::ProgressBar(theScene->percentage, ImVec2(0.0f, 0.0f));

	}
	ImGui::Spacing();

	// Scene benchmark settings
	if (ImGui::CollapsingHeader("Scene Benchmark Setting"))
	{
		static char str0[128] = "";
		ImGui::InputText("Benchmark name", str0, IM_ARRAYSIZE(str0));
		ImGui::InputFloat3("Control Point 1", cp1);
		ImGui::InputFloat3("Control Point 2", cp2);
		ImGui::InputFloat3("Control Point 3", cp3);
		ImGui::InputFloat3("Control Point 4", cp4);
		ImGui::InputFloat("Time taken (s)", &spline_time);
		if (ImGui::Button("Start"))
		{
			cam->createSplineMovement(std::string(str0), cp1, cp2, cp3, cp4, spline_time);
		}
		ImGui::SameLine();
		ImGui::ProgressBar(cam->percentage, ImVec2(0.0f, 0.0f));
	}
	ImGui::Spacing();

	// Past scene benchmarks
	if (ImGui::CollapsingHeader("Past Scene Benchmarks"))
	{
		for (auto &b : cam->benchmarks) {
			ImGui::Text("Name: %s \tMin: %f \tMax: %f \tAverage: %f", b.name.c_str(), b.min, b.max, b.average);
		}
	}

	ImGui::End();
}

// Method to query all of the check boxes and apply correct functionality
void GUI::applyCheckboxes()
{
	// Wireframe
	if (wireframe) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	else {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	// Face Orientation
	if (polygon_orientation == 0) {
		glFrontFace(GL_CW);
	}
	else if(polygon_orientation == 1) {
		glFrontFace(GL_CCW);
	}

	// Backface culling
	if (backface_cull) {
		glEnable(GL_CULL_FACE);
	}
	else {
		glDisable(GL_CULL_FACE);
	}

	// vsync
	if (vsync) {
		glfwSwapInterval(1);
	}
	else {
		glfwSwapInterval(0);
	}
}

int GUI::getProjection()
{
	return cam_projection;
}

float GUI::getFrameTime()
{
	return (1000.0f / ImGui::GetIO().Framerate);
}
