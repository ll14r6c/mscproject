#ifndef IMGUICLASS_H
#define IMGUICLASS_H

#include <glad/glad.h> 

#include <filesystem> // get filenames for model loader

// imgui
#include "imgui.h"
#include "examples/imgui_impl_glfw.h"
#include "examples/imgui_impl_opengl3.h"

#include <GLFW/glfw3.h>

#define GLM_ENABLE_EXPERIMENTAL
#include "glm/gtx/string_cast.hpp"
#include <stdlib.h>

#include "Mesh.h"
#include "Camera.h"
#include "Quadtree.h"
#include "Octree.h"
#include "Scene.h"

class GUI
{
public:
	GUI(GLFWwindow* window, Camera *cam, Scene *myScene);
	~GUI();

	void newFrame();
	void render();
	void toggleOverlay();
	void showOverlay();
	void showMenu();
	void applyCheckboxes();
	int getProjection();
	float getFrameTime();

	Camera *cam;

	Scene *theScene;

	// Light position
	float lightPos[3] = { -1.0f, 0.0f, 0.0f };

	int totalTriangles = 0;
	bool isOverlayVisiable = true;
	bool boundingBox = false;
	bool draw_frustum = false;
	bool draw_quadtree = false;
	bool draw_octree = false;
	int culling_method = 0;


	// Scene settings
	int scene_method = 0;

	// Ordered scene settings
	int ordered_counter = 0;
	int ordered_spacing = 0;
	int ordered_width = 0;
	float ordered_start[3] = { 0.10f, 0.20f, 0.30f };
	int ordered_model_selected = 0;
	std::string ordered_mesh;

	// Random scene settings
	float random_min_position[3] = { 0.0f, 0.0f, 0.0f };
	float random_max_position[3] = { 0.0f, 0.0f, 0.0f };
	int random_total = 0;
	int random_seed = 0;

	// City scene settings
	float city_position[3] = { 0.0f, 0.0f, 0.0f };
	int city_road = 0;
	int city_width = 0;
	bool city_euc = false;
	int city_seed = 0;

	// Benchmark bezier spline settings
	float spline_time = 10.0f;
	float cp1[3] = { 200.0f, 50.0f, 200.0f };
	float cp2[3] = { 100.0f, 12.0f, -200.0f };
	float cp3[3] = { 50.0f, -10.0f, 150.0f };
	float cp4[3] = { 0.0f, 0.0f, 0.0f };

private:
	bool displayOverlay = true;
	const float staticDistance = 5.0f;
	const int staticCorner = 3; // bottom right
	int cam_projection = 0;
	bool wireframe = false;
	bool backface_cull = false;
	bool render_structure = false;
	bool vsync = true;
	int polygon_orientation = 0; // 0 = CW, 1 = CCW
	int model_selected = 0;
	int texture_selected = 0;

	// Default path settings
	std::string model_path = "models/icosphere.obj";
	std::string texture_path = "textures/light.png";
};

#endif